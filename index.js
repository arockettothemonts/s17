/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printOpeningMessage(){
		let montsName = prompt('Enter your full name: ');
		console.log('Hello ' + montsName);

		let montsAge = prompt('Enter your age: ');
		console.log(montsAge + ' years old');

		let montsLocation = prompt('Enter the city where you live: ');
		console.log(montsLocation);
	};

	printOpeningMessage();




/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printIanTopBands(){
		let myMusicians = ['IU','TWICE','Eraserheads','Ben&Ben','Rivermaya']
		console.log(myMusicians);
	};

	printIanTopBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayIanTopMovies(){
		console.log('Film: Broker; RT Rating: 87%');
		console.log('Film: 2012; RT Rating: 39%');
		console.log('Film: 3 Idiots; RT Rating: 100%');
		console.log('Film: Dangal; RT Rating: 88%');
		console.log('Film: The Sound of Music; RT Rating: 83%');
	};

	displayIanTopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

/*let printFriends = */function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();


// console.log(friend1);
// console.log(friend2);
